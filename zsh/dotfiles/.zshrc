# CONFIGS
source $HOME/dotfiles/zsh/configs/index

# THEME
source $ZSH_THEME_DIR/index

#PLUGINS
source $ZSH_PLUGINS_DIR/index

# CUSTOM PATHS
source $HOME/dotfiles/zsh/configs/paths

# ALIASES
source $HOME/dotfiles/aliases/index
